﻿using System.Collections;
using System.Collections.Generic;

namespace Tasks
{
    public class DoubleLinkedListEnumerator<T>:IEnumerator<T>
    {
        private int counter = -1;
        private Node<T> head;
        private Node<T> current;

        public T Current => current.Data;
        object? IEnumerator.Current => Current;

        public DoubleLinkedListEnumerator(Node<T> head)
        {
            this.head = head;
        }     
        
        public bool MoveNext()
        {
            if (counter == -1)
            {
                current = head;
            }
            else
            {
                current = current.Next;
            }

            counter++;

            return current != null;
        }

        public void Reset()
        {
            counter = -1;
        }

        public void Dispose()
        {
        }
    }
}
