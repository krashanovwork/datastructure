﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tasks.DoNotChange;

namespace Tasks
{
    public class DoublyLinkedList<T> : IDoublyLinkedList<T>
    {
        private Node<T> head;
        private Node<T> tail;

        public int Length { get; private set; }

        public void Add(T e)
        {
            var newNode = new Node<T>(e);

            if (head == null)
            {
                head = newNode;
            }
            else
            {
                tail.Next = newNode;
                newNode.Previous = tail;

            }

            tail = newNode;
            Length++;
        }

        public void AddAt(int index, T e)
        {
            var newNode = new Node<T>(e);

            if (head == null)
            {
                Add(e);
            }

            if (index == 0)
            {
                newNode.Next = head;
                head.Previous = newNode;
                head = newNode;
            }

            if (index == Length)
            {
                Add(e);
                return;
            }

            int counter = 0;
            var currentElem = head;

            while (counter < index)
            {
                currentElem = currentElem.Next;
                counter++;
            }

            newNode.Next = currentElem;
            newNode.Previous = currentElem.Previous;

            if (currentElem.Previous != null)
            {
                currentElem.Previous.Next = newNode;
            }

            currentElem.Previous = newNode;
            Length++;
        }

        public T ElementAt(int index)
        {
            if (index < 0 || Length == 0 || index > Length - 1)
            {
                throw new IndexOutOfRangeException();
            }

            int counter = 0;
            var currentElem = head;

            while (counter < index)
            {
                currentElem = currentElem.Next;
                counter++;
            }

            return currentElem.Data;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new DoubleLinkedListEnumerator<T>(head);
        }

        public void Remove(T item)
        {
            var currentElem = head;

            while (currentElem.Next != null && !currentElem.Data.Equals(item))
            {
                currentElem = currentElem.Next;
            }

            if (currentElem.Data.Equals(item))
            {
                if (currentElem.Previous != null)
                {
                    currentElem.Previous.Next = currentElem.Next;
                }

                if (currentElem.Next != null)
                {
                    currentElem.Next.Previous = currentElem.Previous;
                }

                Length--;
            }

            if (currentElem.Previous == null)
            {
                head = currentElem.Next;
            }

        }

        public T RemoveAt(int index)
        {
            if (index < 0 || Length == 0 || index > Length - 1)
            {
                throw new IndexOutOfRangeException();
            }

            int counter = 0;
            var currentElem = head;

            while (counter < index)
            {
                currentElem = currentElem.Next;
                counter++;
            }

            if (currentElem.Next != null)
            {
                currentElem.Next.Previous = currentElem.Previous;
            }

            if (currentElem.Previous != null)
            {
                currentElem.Previous.Next = currentElem.Next;
            }


            Length--;
            return currentElem.Data;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
